<?php

namespace Modules\Product\Repositories\Eloquent;

use Modules\Product\Repositories\ProductRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Modules\Product\Events\ProductWasCreated;
use Modules\Product\Events\ProductWasUpdated;

class EloquentProductRepository extends EloquentBaseRepository implements ProductRepository
{
	/**
     * Paginating, ordering and searching through pages for server side index table
     * @param Request $request
     * @return LengthAwarePaginator
     */
    public function serverPaginationFilteringFor(Request $request): LengthAwarePaginator
    {
        $model = $this->allWithBuilder();

        if ($request->get('search') !== null) {
            $term = $request->get('search');
            $model->where('name', 'LIKE', "%{$term}%")
            	->orwhere('description', 'LIKE', "%{$term}%")
                ->orWhere('id', $term);
        }

        if ($request->get('order_by') !== null && $request->get('order') !== 'null') {
            $order = $request->get('order') === 'ascending' ? 'asc' : 'desc';

            $model->orderBy($request->get('order_by'), $order);
        } else {
            $model->orderBy('id', 'asc');
        }

        return $model->paginate($request->get('per_page', 10));
    }

     /**
     * @param  mixed  $data
     * @return object
     */
    public function create($data)
    {
        $model = $this->model->create($data);
        event(new ProductWasCreated($model, $data));
        return $model;
    }

    /**
     * @param $model
     * @param  array  $data
     * @return object
     */
    public function update($model, $data)
    {
        $model->update($data);
        event(new ProductWasUpdated($model, $data));
        return $model;
    }

    public function apiGetListProducts($request)
    {
        $model = $this->model;
        if (!empty($request->get('search'))) {
            $term = $request->get('search');
            $model = $model->where('name', 'LIKE', "%{$term}%")
                ->orwhere('description', 'LIKE', "%{$term}%")
                ->orWhere('id', $term);
        }
         if (!empty($request->get('order_by'))) {
            $order = $request->get('order') === 'ascending' ? 'asc' : 'desc';
            $model = $model->orderBy($request->get('order_by'), $order);
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }
}
