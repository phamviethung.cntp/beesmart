<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Media\Support\Traits\MediaRelation;

class Product extends Model
{
	use MediaRelation;
    protected $table = 'bm__products';
    protected $fillable = [
    	'name',
    	'price',
    	'description',
    ];

    public function getImageAttribute()
    {
        $image = $this->files()->where('zone', 'image')->first();

        if ($image === null) {
            return '';
        }
        return $image;
    }
}
