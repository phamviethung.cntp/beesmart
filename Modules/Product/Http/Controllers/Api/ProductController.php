<?php

namespace Modules\Product\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Product\Entities\Product;
use Modules\Product\Http\Requests\CreateProductRequest;
use Modules\Product\Http\Requests\UpdateProductRequest;
use Modules\Product\Repositories\ProductRepository;
use Modules\Product\Transformers\Admin\ProductTransformer;
use Modules\Product\Transformers\Api\ApiProductTransformer;
use App\Http\Controllers\ApiBaseController;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class ProductController extends ApiBaseController
{
    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @var Manager
     */
    private $fractal;

    public function __construct(ProductRepository $productRepository, Manager $fractal)
    {
        $this->productRepository = $productRepository;
        $this->fractal = $fractal;
    }

    public function index(Request $request)
    {
        return ProductTransformer::collection($this->productRepository->serverPaginationFilteringFor($request));
    }

    public function store(CreateProductRequest $request)
    {
        $this->productRepository->create($request->all());

        return response()->json([
            'errors' => false,
            'message' => trans('product::products.messages.product created'),
        ]);
    }

    public function find(Product $product)
    {
        return new ProductTransformer($product);
    }

    public function update(Product $product, UpdateProductRequest $request)
    {
        $this->productRepository->update($product, $request->all());

        return response()->json([
            'errors' => false,
            'message' => trans('product::products.messages.product updated'),
        ]);
    }

    public function destroy(Product $product)
    {
        $this->productRepository->destroy($product);

        return response()->json([
            'errors' => false,
            'message' => trans('product::products.messages.product deleted'),
        ]);
    }

    public function list(Request $request)
    {
        //get param query product
        $perPage = $request->has('limit') ? $request->get('limit') : 10;
        $currentPage = $request->has('page') ? $request->get('page') : 1;
        //repository query get product
        $items = $this->productRepository->apiGetListProducts($request);
        //pagination
        $total = count($items->get());
        $data = $items->paginate($perPage);
        $paginate = new Paginator($items, $total, $perPage, $currentPage);
        //response tranfomer with Fractal
        $data = new Collection($data, new ApiProductTransformer());
        $data = $this->fractal->createData($data)->toArray();

        return $this->respondWithPagination($paginate, $data, 'Get list products');
    }
}
