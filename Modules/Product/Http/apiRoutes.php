<?php
use Illuminate\Routing\Router;

/** @var Router $router */
$router->group(['prefix' => '/product', 'middleware' => ['api.token', 'auth.admin']], function (Router $router) {
    $router->get('products', [
        'as' => 'api.product.product.index',
        'uses' => 'ProductController@index',
        'middleware' => 'token-can:product.products.index',
    ]);
    $router->delete('products/{product}', [
        'as' => 'api.product.product.destroy',
        'uses' => 'ProductController@destroy',
        'middleware' => 'token-can:product.products.destroy',
    ]);
    $router->post('products', [
        'as' => 'api.product.product.store',
        'uses' => 'ProductController@store',
        'middleware' => 'token-can:product.products.create',
    ]);
    $router->post('products/{product}', [
        'as' => 'api.product.product.find',
        'uses' => 'ProductController@find',
        'middleware' => 'token-can:product.products.edit',
    ]);
    $router->post('products/{product}/edit', [
        'as' => 'api.product.product.update',
        'uses' => 'ProductController@update',
        'middleware' => 'token-can:product.products.edit',
    ]);
});
//api product public
$router->get('products', [
    'as' => 'api.product.product.list',
    'uses' => 'ProductController@list',
]);