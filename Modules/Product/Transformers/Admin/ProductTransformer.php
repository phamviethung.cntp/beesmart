<?php

namespace Modules\Product\Transformers\Admin;

use Illuminate\Http\Resources\Json\Resource;

class ProductTransformer extends Resource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'price' => (int) $this->price,
            'description' => $this->description,
            'created_at' => $this->created_at->format('d-m-Y'),
            'urls' => [
                'delete_url' => route('api.product.product.destroy', $this->id),
            ],
        ];
    }
}
