<?php

namespace Modules\Product\Transformers\Api;

use League\Fractal\TransformerAbstract;
use Modules\Media\Image\Imagy;
use Modules\Product\Entities\Product;

class ApiProductTransformer extends TransformerAbstract
{
    public function transform(Product $product)
    {
        $imagy = app(Imagy::class);
        $image = !empty($product->getImageAttribute()) ? $product->getImageAttribute() : '';
        return [
            'id' => $product->id,
            'name' => $product->name,
            'price' => (int)$product->price,
            'description' => $product->description,
            'image' => $product->image,
            'created_at' => $product->created_at->format('d-m-Y'),
        ];
    }
}
