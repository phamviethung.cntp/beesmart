<?php

return [
    'list resource' => 'List products',
    'create resource' => 'Create products',
    'edit resource' => 'Edit products',
    'destroy resource' => 'Destroy products',
    'title' => [
        'products' => 'Products',
        'create product' => 'Create a product',
        'edit product' => 'Edit a product',
    ],
    'button' => [
        'create product' => 'Create a product',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
        'product created' => 'Product successfully created.',
        'product not found' => 'Product not found.',
        'product updated' => 'Product successfully updated.',
        'product deleted' => 'Product successfully deleted.',
    ],
    'validation' => [
    ],
];
